# Arduino CNC plotter #

This project uses scrap DVD drive's stepper motor with L293D Motor Driver Shield

## Setup ##

This project consists of 4 main parts:
1. Inkscape module
2. Processing software
3. Arduino software
4. Scematic

Install them in any order, description in the corersponding part.

## Usage ##
### 1. Image -> gcode ###
First use the inkscape module to creategcode from the paths in a given image.
> **Note:** The you need to vecorise than convert to path any image/drawing you want to use.
### 2. Start arduino ###
Start and connect arduino **and** the power source to separate USB plugs.
### 3. Gcode -> Arduino ###
Use the processing program to send the gcode to the arduino

