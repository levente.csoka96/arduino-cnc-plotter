#include <Servo.h>
#include <AFMotor.h>

char      STEP               = MICROSTEP;
const int penZUp             = 10;
const int penZDown           = 83;
const int penServoPin        = 10 ;
const int stepsPerRevolution = 48; 

Servo      penServo;  
AF_Stepper myStepperY(stepsPerRevolution,1);            
AF_Stepper myStepperX(stepsPerRevolution,2);  

struct point
{ 
  float x; 
  float y; 
  float z; 
};

struct point actuatorPos;
float StepInc   = 1;
int   StepDelay = 0;
int   LineDelay = 0;
int   penDelay  = 10;

// Motor steps to go 1 millimeter
float StepsPerMillimeterX = 2.9;
float StepsPerMillimeterY = 2.9;

float Xmin = 0;
float Xmax = 30;
float Ymin = 0;
float Ymax = 30;
float Zmin = 0;
float Zmax = 1;

float Xpos = 0;
float Ypos = 0;
float Zpos = Zmax; 

/**********************
 * void setup() - Initialisation
 ***********************/

void setup() {
  Serial.begin( 9600 );
  penServo.attach(penServoPin);
  penServo.write(penZUp);
  myStepperX.setSpeed(10);
  myStepperY.setSpeed(10);  
  Serial.println("CNC Plotter ready!");
}


/**********************
 * void loop() - Main loop
 ***********************/

void loop() 
{
  while (1)
  {
    while(Serial.available())
    {
      String line = Serial.readString();
      if(line[0] != '(') //discard if this is a comment
      {
        processIncomingLine(line);
      }
      Serial.println("ok"); 
    }
  }
}

void processIncomingLine(String line)
{
  struct point newPos;
  newPos.x = 0.0;
  newPos.y = 0.0;
  
  switch ( line[0] )
  {
    case 'U':
      penUp(); 
      break;
    case 'D':
      penDown(); 
      break;
  case 'G':
    switch (line.substring(1,3).toInt())
    {
      case 0:
      case 1:
        String command = line.substring(line.indexOf(' ') + 1);
        String fcom = command.substring(0, command.indexOf(' '));
        String scom = command.substring(fcom.length() + 1, command.indexOf(' ',fcom.length() + 1));
        newPos.x = actuatorPos.x;
        newPos.y = actuatorPos.y;
        switch(fcom[0])
        {
          case 'X':
            newPos.x = fcom.substring(1).toFloat();
            break;
          case 'Y':
            newPos.y = fcom.substring(1).toFloat();
            break;
          default:
          break;
        }
        switch(scom[0])
        {
          case 'X':
            newPos.x = scom.substring(1).toFloat();
            break;
          case 'Y':
            newPos.y = scom.substring(1).toFloat();
            break;
          default:
          return;
          break;
        }
        drawLine(newPos.x, newPos.y);
        actuatorPos.x = newPos.x;
        actuatorPos.y = newPos.y;
      break;
    }
    break;
  case 'M':
    String command = line.substring(0, line.indexOf(' '));
    switch(command.substring(1).toInt())
    {
      case 300:
        // we use the second word, but discard the letter S from the start of it
        int degreee = line.substring(command.length() + 2,
                                     line.indexOf(' ', command.length() + 1)).toInt();
        if(degreee == 255) degreee = penZUp; // TODO: remove this dirty solution
        penServo.write(degreee); 
        delay(penDelay); 
      break;
      default:
      break;
    }
  }
}


void drawLine(float x1, float y1) {

  if(y1 < 0) y1 += Ymax; // TODO: remove this dirty solution
  
  if (x1 >= Xmax) x1 = Xmax; 
  if (x1 <= Xmin) x1 = Xmin;
  if (y1 >= Ymax) y1 = Ymax;
  if (y1 <= Ymin) y1 = Ymin; 

  x1 = (int)(x1*StepsPerMillimeterX);
  y1 = (int)(y1*StepsPerMillimeterY);
  float x0 = Xpos;
  float y0 = Ypos;

  long dx = abs(x1-x0);
  long dy = abs(y1-y0);
  int sx = x0<x1 ? FORWARD : BACKWARD;
  int sy = y0<y1 ? BACKWARD : FORWARD;

  long i;
  long over = 0;

  if (dx > dy)
  {
    for (i=0; i<dx; ++i)
    {
      myStepperX.step(1,sx,STEP);
      over+=dy;
      if (over>=dx)
      {
        over-=dx;
        myStepperY.step(1,sy,STEP);
      }
    delay(StepDelay);
    }
  }
  else
  {
    for (i=0; i<dy; ++i)
    {
      myStepperY.step(1,sy,STEP);
      over+=dx;
      if (over>=dy)
      {
        over-=dy;
        myStepperX.step(1,sx,STEP);
      }
      delay(StepDelay);
    }    
  }
  delay(LineDelay);
  Xpos = x1;
  Ypos = y1;
}

void penUp()
{ 
  penServo.write(penZUp); 
  delay(penDelay); 
  Zpos=Zmax; 
}


void penDown()
{ 
  penServo.write(penZDown); 
  delay(penDelay); 
  Zpos=Zmin; 
}
